Project Alavi Beta 
Integrantes Jhony Atertua,Anna Melendez , Miguel Angel Rico y Juan Camilo Lasso
Instrucciones del Juego:
Inicialmente el personaje aparecerá en un Lobby con 5 puertas, el podrá pasar por todas la puertas
El astronauta se puede mover con W y S para avanzar o retroceder, A y D y con Space puede utilizar las botas cohete, la interacción con los objetos funciona presionando la letra E
Al darle ESC y selecciona el botón lobby, se puede ir directamente a la escena de Lobby donde hay acceso de los demas nivel
Niveles
1.La idea de este nivel es moverte por un laberinto para obtener unos puntos rojos, aquí tienes la oportunidad de utilizar botas cohete e impulsar al jugador ha plataformas ;y así abrir la puerta al siguiente nivel
Para utilizar las botas cohete se debe presionar SPACE
Para obtener los puntos solo debes tocarlos
Al obtener todos los puntos se abrirá una puerta, lo que corresponde a ganar el nivel
Seguir el recorrido del laberinto te llevará a cada uno de los recolectables
2. Debes organizar las piezas de tal manera en que sean iguales a la línea blanca que está en el piso
Hay 3 botones, de color amarillo, morado y azul, te debes parar enfrente de esos botones y presionar E para que las piezas se muevan
Si todas las piezas encajan el patrón del piso , se abrira una puerta roja que permitirá pasar al siguiente nivel
3.El jugador debe agarrar objetos y los coloque en su color correspondiente, habrán unos ocultos y otros en plataformas por lo tanto deberá usar las botas cohete, cuando complete esta tarea se abrirá la puerta al nivel ganador.
 
Para utilizar las Botas Cohete presionar SPACE
   Para agarrar los objetos utilizar la tecla E
   Debes acomodar todos los objetos para pasar a la siguiente pantalla
Bugs:
Aún tenemos problemas con el personajes, en específico con el modelo, cuando usa las botas collider no se puede rotar el personaje en el aire
A veces toca volver a comprar las habilidades o acercarse nuevamente específicamente la collision, por lo tanto hay problemas de colliders
En el Primer nivel el jugador puede atravesar un muro siendo que tiene un collider
En el segundo, el último objeto para recoger a veces no entiende que el objeto lo está tocando siendo que comparte interacción con los otros






Links de ASSETS 
https://quaternius.itch.io/lowpoly-robot
https://assetstore.unity.com/packages/3d/environments/low-poly-pack-94605
https://assetstore.unity.com/packages/3d/environments/sci-fi/sci-fi-doors-162876
https://assetstore.unity.com/packages/3d/environments/sci-fi/sci-fi-styled-modular-pack-82913
https://assetstore.unity.com/packages/3d/environments/low-poly-nature-collection-129653


Links de SONIDOS
https://www.soundboard.com/sb/sound/58599
https://www.free-stock-music.com/tubebackr-space-race.html
https://www.soundboard.com/sb/sound/58599
https://www.zapsplat.com/sound-effect-category/door/page/2/
https://freesound.org/people/KieranKeegan/sounds/418881/ https://freesound.org/people/TreasureSounds/sounds/332629/ https://freesound.org/people/dermotte/sounds/263001/ https://freesound.org/people/jacksonacademyashmore/sounds/402816/ https://freesound.org/people/jalastram/sounds/386582/

