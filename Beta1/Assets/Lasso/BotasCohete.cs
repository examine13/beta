﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotasCohete : MonoBehaviour
{
    [SerializeField]
    private Image BotsFuel;

    [SerializeField]
 
    Animator MyAnim;
    private GameObject monedero;
    private TiendaHabilidades tienda;
    public float speed = 3;
    public CharacterController CharControl;
    public Vector3 currentvector = Vector3.up;
    public ParticleSystem Particle;
    public Light Luz;
    public AudioSource Sonidos;
    public float fuel;
    [SerializeField]
    private float fuelmaximo;
    public float Fuerza;
    [SerializeField]
    private float FuerzaMaxima;
    [SerializeField]
    private float ForceDelta;
    private bool Press;
    private Player jugador;

    // Start is called before the first frame update
    void Start()
    {
        jugador = GetComponent<Player>();
        monedero = GameObject.FindWithTag("Monedero");
        if(monedero!= null)
        {
            tienda = monedero.GetComponent<TiendaHabilidades>();
        }
    }
    private void Update()
    {
        Press = Input.GetKey(KeyCode.LeftShift);
        Particle.gameObject.SetActive(Press);
            
        
        print(Press);

        if (Press && fuel > 1 && Fuerza == 0)
        {
            Fuerza = jugador.gravity;
        }
        if (Press && fuel > 0  )
        {
            Fuerza += ForceDelta;
            Fuerza = Mathf.Clamp(Fuerza, 0f, FuerzaMaxima);
            AntiGravity();

        }
        else if (Fuerza > 0)
        {
            Fuerza -= ForceDelta*2;
            Fuerza = Mathf.Clamp(Fuerza, 0f, FuerzaMaxima);
            AntiGravity();
        }
        if (Press && fuel > 0)
        {
            fuel -= Time.deltaTime*4;
        }
        else if (fuel < fuelmaximo)        
        {
            fuel += Time.deltaTime;
        }
        fuel = Mathf.Clamp(fuel, 0f , fuelmaximo);
        BotsFuel.fillAmount = fuel/3;
        
    }
    private void AntiGravity()
    {
        jugador.FuerzasExternas = -Fuerza;
    }


}