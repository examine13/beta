﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioCajas : MonoBehaviour
{

    public AudioSource Sonido;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerInteractionZone")
        {
            Sonido.Play();
        }
    }

}
