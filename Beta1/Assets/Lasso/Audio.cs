﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    public AudioSource Sonido;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Sonido.Play();
        }
    }
}
