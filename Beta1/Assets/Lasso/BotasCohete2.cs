﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotasCohete2 : MonoBehaviour
{
    private GameObject monedero;
    private TiendaHabilidades tienda;

    [SerializeField]
    private Image BotsFuel;
    Rigidbody rb;
    [SerializeField]
    private float UseTime = 0;
    [SerializeField]
    private float MAxUseTime = 1;
    Animator MyAnim;

    // Start is called before the first frame update
    void Start()
    {
        rb = transform.root.GetComponent<Rigidbody>();
        MyAnim = GetComponent<Animator>();
        animacion();
        monedero = GameObject.FindWithTag("Monedero");
        if(monedero != null)
        {
            tienda = monedero.GetComponent<TiendaHabilidades>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (tienda.onCohete)
        {
            Cohete();
            animacion();
            Debug.Log(tienda.onCohete);
        }


    }

    private void animacion()
    {
        BotsFuel.fillAmount = (MAxUseTime - UseTime);

    }
    private void Cohete()
    {


        if (UseTime <= MAxUseTime)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                rb.AddForce(0, 13f, 0);
                UseTime += Time.deltaTime;
            }
            else
            {
                UseTime -= Time.deltaTime;
            }

        }
        else
        {
            UseTime = MAxUseTime;

        }






        if (UseTime < 0)
        {
            UseTime = 0;
        }


    }
}


