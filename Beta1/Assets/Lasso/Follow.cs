﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour

{
    public GameObject Botas;
    public CharacterController Char;
    Vector3 posicion;

    void Update()
    {
        if (Char.isGrounded == false && Input.GetKeyDown(KeyCode.LeftShift))
        {
            seguir();
        }

    }

    private void seguir()
    {
        posicion = Botas.transform.position;
        Char.transform.position = posicion;
    }

}
