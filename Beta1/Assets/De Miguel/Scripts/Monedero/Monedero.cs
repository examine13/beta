﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monedero : MonoBehaviour
{
    static Monedero current;

    public int ContMonedas;


    private void Awake()
    {
        if(current != null && current != this)
        {
            Destroy(gameObject);
            return;
        }
        current = this;
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        ContMonedas = PlayerPrefs.GetInt("NumMonedas");
        EventoColission._CogerMoneda += SumaMonedas;
    }

    public void SumaMonedas()
    {
        ContMonedas++;
    }
    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("NumMonedas", ContMonedas);
    }
}
