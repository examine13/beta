﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Habilidad : MonoBehaviour
{
    [SerializeField]
    public int type;

    GameObject monedero;
    TiendaHabilidades tienda;

    void Start()
    {
        monedero = GameObject.FindWithTag("Monedero");
        if (monedero != null)
        {
            tienda = monedero.GetComponent<TiendaHabilidades>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (type == 1 && tienda.onAgarrar == true)
        {
            this.gameObject.transform.position = Vector3.zero;
        }if(type == 2 && tienda.onCohete == true)
        {
            this.gameObject.transform.position = Vector3.zero;
        }
        if(type == 3 && tienda.onFuerza == true)
        {
            this.gameObject.transform.position = Vector3.zero;
        }
    }
    /*private void OnCollisionEnter(Collision collision)
    {
        EventoColission._Compra();
        if (type <= 3 && type >= 1)
        {
            EventoColission._tipoHabilidad = type;
        }
        else
        {
            EventoColission._tipoHabilidad = 0;
        }

    }*/
    private void OnTriggerEnter(Collider other)
    {
        EventoColission._Compra();
        if (type <= 3 && type >= 1)
        {
            EventoColission._tipoHabilidad = type;
        }
        else
        {
            EventoColission._tipoHabilidad = 0;
        }
    }
}
