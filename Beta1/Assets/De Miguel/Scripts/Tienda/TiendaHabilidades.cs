﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiendaHabilidades : MonoBehaviour
{
    static TiendaHabilidades current;
    [SerializeField]
    int precioAgarrar, precioCohete, precioFuerza;
    public bool onCohete, onAgarrar, onFuerza;

    [SerializeField]
    Monedero monedero;
    /*
    public bool OnCohete { get => onCohete; }
    public bool OnAgarrar { get => onAgarrar; }
    public bool OnFuerza { get => onFuerza; }
    */
    private void Awake()
    {
        if (current != null && current != this)
        {
            Destroy(gameObject);
            return;
        }
        current = this;
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        EventoColission._Compra += Compra;
    }

    public void Compra()
    {

        //Habilidad 1 Agarrar, 2 Cohetes, 3 Fuerza
        if(EventoColission._tipoHabilidad == 1 && monedero.ContMonedas >= precioAgarrar)
        {
            monedero.ContMonedas -= precioAgarrar;
            onAgarrar = true;
        }
        if(EventoColission._tipoHabilidad == 2 && monedero.ContMonedas >= precioCohete)
        {
            monedero.ContMonedas -= precioCohete;
            onCohete = true;
        }
        if(EventoColission._tipoHabilidad == 3 && monedero.ContMonedas >= precioFuerza)
        {
            monedero.ContMonedas -= precioFuerza;
            onFuerza = true;
        }
        else
        {

        }

    }
    private void Update()
    {

    }
}
