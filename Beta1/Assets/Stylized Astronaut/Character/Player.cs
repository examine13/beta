using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{

    private Animator anim;
    private CharacterController controller;
    
    public float speed = 600.0f;
    public float turnSpeed = 400.0f;
    private Vector3 moveDirection;
    public float gravity = 30.0f;
    public float FuerzasExternas = 0;
    public float jumpspeed = 15.0f;
    private Vector3 PaArriba = Vector3.up;

    void Start()
    {
      
        controller = GetComponent<CharacterController>();
        anim = gameObject.GetComponentInChildren<Animator>();
    }

    private void FixedUpdate()
    {
        if (Input.GetKey("w"))
        {
            anim.SetInteger("AnimationPar", 1);
        }
        else
        {
            anim.SetInteger("AnimationPar", 0);
        }

        if (controller.isGrounded)
        {
            moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed; 
        float turn = Input.GetAxis("Horizontal");
        transform.Rotate(0, turn * turnSpeed * Time.deltaTime, 0);

            if (Input.GetButton("Jump"))
            {
                moveDirection.y += jumpspeed;
            }

        }
        else
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), moveDirection.y, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = new Vector3(moveDirection.x*4f,moveDirection.y,moveDirection.z*speed); 
        }
       


        moveDirection.y -= (gravity + FuerzasExternas) * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
        

    }
}
    

